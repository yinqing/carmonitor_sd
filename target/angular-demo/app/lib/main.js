window.onresize = function(){
	var menuWidth = $(".main-menu").width();
	$(".main-content").css({
			"height":"calc(100% - 44px)",
			"height":"-moz-calc(100% - 44px)",
			"height":"-webkit-calc(100% - 44px)",
			"width":"calc(100% - "+menuWidth+"px)",
			"width":"-moz-calc(100% - "+menuWidth+"px)",
			"width":"-webkit-calc(100% - "+menuWidth+"px)",
		});
}

//弹窗事件
function layer_show(w,h,title,url){
	if (w == null || w == '') {
		w=800;
	};
	if (h == null || h == '') {
		h=($(window).height() - 50);
	};
	if (title == null || title == '') {
		title=false;
	};
	if (url == null || url == '') {
		url="404.html";
	};
	layer.open({
    	type: 2,
    	shadeClose: false,
    	title: title,
		maxmin:false,
		//shadeClose: true,
//    	closeBtn: [0, true],
    	shade: [0.8, '#000'],
    	border: [0],
    	offset: ['20px',''],
    	area: [w+'px', h +'px'],
    	content:url
	});
}

//阻止冒泡事件
function stopPropagation(e){
	e = window.event||e;
	if(document.all){
		e.cancelBubble = true;
	}else{
		e.stopPropagation();
	}
}
//iframe中关闭当前窗口，并给出弹框提示
function refreshTable(msg){
	var index = parent.layer.getFrameIndex(window.name);  
     parent.layer.close(index);  
     parent.layer.alert(msg);
     parent.$("#table").bootstrapTable("refresh");
}
//关闭下拉框
$("body").click(function(e){
    if($(e.target).closest('.autoSelect-input').length){          //判断点击的地方是否是在弹出框里面
            //判断点击对象是否在#box内
    }else{
            $(".autoSelect-ul").hide();
    } 
})              
