 layui.define(['layer', 'form'], function(exports){
	var	 $ = layui.$,
  		layer = layui.layer,
	  	form = layui.form;
	  	
	 //加载菜单
	 $.ajax({
	 	type:"get",
	 	url:"./datas/menu.json",
	 	success:function(resp){
	 		var menu = resp.menu;
		 	for(var i=0;i<menu.length;i++){
		 		var index = menu[i].index,
		 			name = menu[i].name;
		 		//加载顶部导航栏
		 		$(".header-items").append('<li class="layui-nav-item"><a href="javascript:;" lay-index='+index+'>'+name+'</a></li>');
		 		//判断是否有子节点
		 		var open = i===0?" layui-nav-itemed":"";
		 		var li = $("<li class='layui-nav-item"+open+"'></li>");
		 		if(menu[i].children.length>0){
		 			var _children = menu[i].children,
		 				name = menu[i].name;
					$(li).append('<a class="menu-item" href="javascript:;" lay-index='+index+'>'+name+'</a>');
					var dl = $("<dl class='layui-nav-child'></dl>");
					for(var j=0;j<_children.length;j++){
						var url = _children[j].url,
							name = _children[j].name;
						var checked = (i===0&&j===0)?"layui-this":"";
						$(dl).append('<dd class='+checked+'><a href="javascript:;" lay-url='+url+'>'+name+'</a></dd>');
					}
					$(li).append(dl);
		 		}else{
		 			var url = menu[i].url;
		 			$(li).append('<a class="menu-item" href="javascript:;" lay-index='+index+' lay-url='+url+'>'+name+'</a>')
		 		}
		 		$(".layui-nav-tree").append(li);
		 	}
		 	layui.use('element', function(){
			  var element = layui.element;
			});
	 	},
	 	error:function(){
	 		layer.msg("加载失败。。。");
	 		return;
	 	}
	 });
  	//头部导航菜单点击事件
  	$(".header-items").on("click",".layui-nav-item",function(){
  		//获取点击导航栏序号
		var index = $(this).find("a").attr("lay-index");
		var leftMenu = $(".layui-nav-tree").find("li");
		for(var i=0;i<leftMenu.length;i++){
			var _index = $(leftMenu[i]).find(".menu-item").attr("lay-index");
			if(index === _index && $(leftMenu[i]).find(".menu-item").siblings("dl").length>0) {
				$(leftMenu[i]).find("dl dd:first").trigger("click");
			}else if(index===_index && $(leftMenu[i]).find(".menu-item").siblings("dl").length===0){
				$(leftMenu[i]).find(".menu-item").trigger("click");
			}
		}
  	});
  	//左侧菜单点击事件
	 $(".layui-nav-tree").on("click",".menu-item",function(){
	 	//点击左侧一级菜单的时候，判断是否有二级菜单，如果有展开，并关闭其他的一级菜单
	 	var _child = $(this).siblings(".layui-nav-child");
	 	if(_child.length===1){
	 		$(this).parent().siblings().removeClass("layui-nav-itemed");
	 	}else{
	 		var index = $(this).attr("lay-index"),
	 			 url = $(this).attr("lay-url");
 			$(this).addClass("layui-this");
 			 //导航栏样式
		 	headNavStyle(index);
		 	//加载页面
	 		loadPageFn(url);
	 	}
	 });
	 //二级菜单点击事件（加载右侧列表）
	 $(".layui-nav-tree").on("click","dd",function(){
	 	var url = $(this).find("a").attr("lay-url");
	 	$(this).parents("li").siblings().removeClass("layui-nav-itemed");
	 	$(this).parents("li").addClass("layui-nav-itemed");
	 	var index = $(this).parent().siblings(".menu-item").attr("lay-index");
	 	 //导航栏样式
	 	headNavStyle(index);
	 	//加载页面
	 	loadPageFn(url);
	 });
	 //退出登录
	 $(".login-out").click(function(){
	 	layer.confirm("是否确认退出？",function(){
	 		layer.msg("退出了")
	 	});
	 });
  //加载右侧列表
  function loadPageFn(url){
  	$.ajax({
		type:"get",
		url:"pages/page1.html",
		success:function(data){
			$("#mainBox").html(data);
		},
		error:function(){
			$.get("404.html",function(data){ //初始將a.html include div#iframe
		　　　　$("#mainBox").html(data);
		　　	}); 
		}
	});
  }
  //导航栏样式
  function headNavStyle(index){
  	$(".layui-nav-item").removeClass("layui-this");
 	var headerNav = $(".header-items").find("a");
 	for(var i=0;i<headerNav.length;i++){
 		var _index = $(headerNav[i]).attr("lay-index");
 		if(index === _index){
 			$(headerNav[i]).parent().addClass("layui-this");
 		}
 	}
  }
  exports("main",{})
});    
