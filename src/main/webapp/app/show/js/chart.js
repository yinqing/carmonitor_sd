var personChart = echarts.init(document.getElementById('person-chart-id'));
var eqChart = echarts.init(document.getElementById('eq-chart-id'));
var towerChart = echarts.init(document.getElementById('tower-charts-id'));
//人员信息
 var option1 = {
    title : {
        text: '人员信息',
        textStyle: {  
            fontWeight: 'normal',              //标题颜色  
            color: '#FFFFFF'  
        },  
        x:'center'
    },
    tooltip : {
        trigger: 'item'
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: ['正常','报警','违规'],
        textStyle: {  
            fontWeight: 'normal',              //标题颜色  
            color: '#FFFFFF'  
        }, 
    },
    series : [
        {
            name: '人员信息',
            type: 'pie',
            radius : '75%',
            center: ['50%', '60%'],
            data:[
                {value:335, name:'正常'},
                {value:310, name:'报警'},
                {value:1548, name:'违规'}
            ],
            label:{
				normal:{
					show: true,
					position: "inside",
					textStyle: {
			　　　　　　color: '#FFFFFF'
			　　　　 }, 
					formatter: function(params,value,obj){
						return params.value;
					}
				}
			},
            color:['green', '#f39015','red']  ,
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};
//终端信息
var option2 =  {
    title : {
        text: '终端信息',
        textStyle: {  
            fontWeight: 'normal',              //标题颜色  
            color: '#FFFFFF'  
        },  
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: ['正常','低电量','离线'],
        textStyle: {  
            fontWeight: 'normal',              //标题颜色  
            color: '#FFFFFF'  
        }
    },
     series : [
        {
            name: '终端信息',
            type: 'pie',
            radius : '75%',
            center: ['50%', '60%'],
            data:[
                {value:235, name:'正常'},
                {value:110, name:'低电量'},
                {value:50, name:'离线'}
            ],
            color:['green', '#f39015','gray']  ,
            label:{
				normal:{
					show: true,
					position: "inside",
					textStyle: {
			　　　　　　color: '#FFFFFF'
			　　　　 },
					formatter: function(params,value,obj){
						return params.value;
					}
				}
			},
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};
//杆塔信息
var option3 = {
    title: {
        text: '杆塔信息',
        x:'center',
        textStyle: {  
            fontWeight: 'normal',              //标题颜色  
            color: '#FFFFFF'  
        } 
    },
    grid:{
    	borderWidth:0,
    },
    tooltip: {
    },
     legend: {
        data: ["当天已检修","当月已检修","剩余未检修"],
        left:'left',
        orient:'vertical',
        textStyle: {  
            fontWeight: 'normal',              //标题颜色  
            color: '#FFFFFF'  
        }
    },
    xAxis: {
        data: ["当天已检修","当月已检修","剩余未检修"],
	    axisLine:{  
            lineStyle:{  
                color:'#FFFFFF',  
            }  
        },  
        axisLabel:{
        	interval:0,
			rotate: 0
        },
	    splitLine:{  
        　　　　show:false  
        　　 } 
    },
       
    yAxis: [{
    	splitLine:{  
                    　　　　show:false  
            　 },
      	axisLine:{  
            lineStyle:{  
                color:'#FFFFFF',  
            }  
        } 
    }],
    series: [{
        name: '当天已检修',
        type: 'bar',
        data: [15,'-','-'],
        barGap:"-100%",
        barWidth :50,
        //配置样式
        itemStyle: {   
            //通常情况下：
            normal:{  
　　　　　　　　　//每个柱子的颜色即为colorList数组里的每一项，如果柱子数目多于colorList的长度，则柱子颜色循环使用该数组
                color: ["green"],
                label: {
                        show: true,
                        position: 'top',
                        textStyle:{
                        	color:"#FFF"
                        }
                    }
            },
            //鼠标悬停时：
            emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        }
    },
    {
        name: '当月已检修',
        type: 'bar',
      	barWidth :50,
        data: ['-',20,'-'],
        barGap:"-100%",
        //配置样式
        itemStyle: {   
            //通常情况下：
            normal:{  
　　　　　　　　　//每个柱子的颜色即为colorList数组里的每一项，如果柱子数目多于colorList的长度，则柱子颜色循环使用该数组
                color: ["blue"],
                label: {
                        show: true,
                        position: 'top',
                        textStyle:{
                        	color:"#FFF"
                        }
                    }
            },
            //鼠标悬停时：
            emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        }
    },
    {
        name: '剩余未检修',
        type: 'bar',
      	barWidth :50,
        data: ['-','-',12],
        barGap:"-100%",
        //配置样式
        itemStyle: {   
            //通常情况下：
            normal:{  
　　　　　　　　　//每个柱子的颜色即为colorList数组里的每一项，如果柱子数目多于colorList的长度，则柱子颜色循环使用该数组
                color: ["red"],
                label: {
                        show: true,
                        position: 'top',
                        textStyle:{
                        	color:"#FFF"
                        }
                    }
            },
            //鼠标悬停时：
            emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        }
    }]
};
 personChart.setOption(option1);
 eqChart.setOption(option2);
 towerChart.setOption(option3);
 
window.onresize = function(){
	personChart.resize();
	eqChart.resize();
	towerChart.resize();
	
	personChart.setOption(option1);
	eqChart.setOption(option2);
	towerChart.setOption(option3);
}
