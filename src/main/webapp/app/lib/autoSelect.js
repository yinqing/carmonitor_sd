(function($){
	var $this  = this;
	$.fn.autoSelect = function(params){
		params = $.extend({
			data:[],
			showDropDown:true,
			change:function(){}
		},params);
		var inputDiv = $("<input type='text' class='autoSelect-input' data-value=''><span class='autoSelect-arrow'><i class='fa fa-caret-down'></i></span>");
		var ul = $("<ul class='autoSelect-ul auto-hide'></ul>");
		for(var i=0;i<params.data.length;i++){
			ul.append("<li data-id='"+params.data[i].id+"'>"+params.data[i].name+"</li>");
		}
		this.append(inputDiv);
		$(ul).css("min-width",$(".autoSelect-input").width()+$(".autoSelect-arrow").width())
		this.append(ul);
		
		this.keyup(function(e){
			ul.empty();
			var value = e.target.value.trim();
			var arr = params.data.filter(function(obj){
				return obj.name.indexOf(value)!=-1;
			});
			if(arr.length!=0){
				for(var i=0;i<arr.length;i++){
					ul.append("<li data-id='"+arr[i].id+"'>"+arr[i].name+"</li>");
				}
				$(ul).slideDown(100);
			}else{
				$(ul).hide();
			}
			$(".autoSelect-input").attr("data-value","");
		});
		
		$(".autoSelect-arrow").click(function(e){
			e = window.event||e;
			ul.empty();
			for(var i=0;i<params.data.length;i++){
				ul.append("<li data-id='"+params.data[i].id+"'>"+params.data[i].name+"</li>");
			}
			var flag = $(ul).is(":visible");
			if(flag){
				$(ul).hide();
			}else{
				$(ul).slideDown(100);
			}
			e.stopPropagation();
		});
		$(".autoSelect-ul").on("click","li",function(e){
			var name = $(this).text(),
				id = $(this).data("id");
			var obj = {
				id:id,
				name:name
			}
			$(".autoSelect-input").attr("data-value",id);
			$(".autoSelect-input").val(name);
			params.change(obj);
		});
		$(".autoSelect-input").change(function(){
			console.log($(this).val());
		});
	}
})(jQuery)

