package com.car.service;

import com.car.dao.IPeopleDao;
import com.car.dao.IRouteDao;
import com.car.dao.ITowerDao;
import com.car.vo.PeopleVO;
import com.car.vo.RouteVO;
import com.car.vo.TowerVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/10/23 0023.
 */
@Service
public class TowerService {
    @Autowired
    private ITowerDao towerDao;
    @Autowired
    private IRouteDao routeDao;
    @Autowired
    private IPeopleDao peopleDao;
    public List<TowerVO> selectAllTower(TowerVO towerVO) throws Exception{
        List<TowerVO> list = towerDao.selectAllTower(towerVO);
        if(null!=list && list.size()>0){
            for(int i=0;i<list.size();i++){
                //获取杆塔的ID
                int id = list.get(i).getId();
                //查询线路名称和巡检人员
               List<RouteVO> routeList = routeDao.selectRouteByTowerId(id);
                routeList.removeAll(Collections.singleton(null));
                String routeName = "";
                if(null!=routeList && routeList.size()>0){
                        for(int j=0;j<routeList.size();j++){
                            String rName = routeList.get(j).getName();
                            if(null!=rName && rName.length()>0){
                                routeName += routeList.get(j).getName()+";";
                            }
                        }
                }
                list.get(i).setRouteNames(routeName);
                String personName = "";
                List<PeopleVO> personList = peopleDao.selectPersonByTowerId(id);
                personList.removeAll(Collections.singleton(null));
                if(null!=personList && personList.size()>0){
                    for(int k=0;k<personList.size();k++){
                        String pName = personList.get(k).getName();
                        if(null!=pName && pName.length()>0){
                            personName += personList.get(k).getName()+";";
                        }
                    }
                }
                list.get(i).setPersonNames(personName);
                //查看杆塔巡检状态(注:线路有可能未完成,但是这个杆塔已经巡检了）
               List<RouteVO> statusList =  routeDao.selectRouteStatusByTowerId(id);
               statusList.removeAll(Collections.singleton(null));
                if(null!=statusList && statusList.size()>0){
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String status = "";
                    for(int m=0;m<statusList.size();m++){
                        Date reachTime = statusList.get(m).getReachTime();
                        String rName = statusList.get(m).getName();
                        String pName = statusList.get(m).getPerspnName();
                        if(null!= reachTime&& !"".equals(reachTime)){
                           String rDate = formatter.format(reachTime);
                           status += rName+"@"+pName+"@"+rDate+"#";
                        }else{
                            status += rName+"@"+pName+"@"+reachTime+"#";
                        }
                    }
                    if(status.length()>0){
                        status = status.substring(0,status.length()-1);
                    }
                    list.get(i).setReachInfo(status);
                }

            }

        }
        return list;
    }

    public void addTower(TowerVO towerVO) throws Exception{
        towerDao.addTower(towerVO);
    }

    public void updateTower(TowerVO towerVO) throws Exception{
        towerDao.updateTower(towerVO);
    }

    public void deleteTowerById(int tId) throws Exception{
        towerDao.deleteTowerById(tId);
    }

    public List<TowerVO> selectTowerByRouteId(int routeId) throws Exception {
        List<TowerVO> list = towerDao.selectTowerByRouteId(routeId);
        return list;
    }

    public List<TowerVO> selectTowerByTaskId(Integer taskId) throws Exception{
        List<TowerVO> list = towerDao.selectTowerByTaskId(taskId);

        return list;
    }
}
