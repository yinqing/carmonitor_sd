package com.car.service;

import com.car.dao.ITrapDao;
import com.car.vo.TrapVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 电子围栏
 * Created by yinqing on 2017/10/25.
 */
@Service
public class TrapService {
    @Autowired
    private ITrapDao trapDao;

    /**
     * 添加围栏信息
     * @param trapVO
     * @return
     * @throws Exception
     */
    public int addTrap(TrapVO trapVO) throws Exception{
        int num = 0;
        //围栏类型：0 圆圈；1 长方形；2多边形'
        int perperty = trapVO.getPerperty();
        trapDao.addTrap(trapVO);
        int addId = trapVO.getId();
        if(perperty==0){
            trapDao.addCircle(trapVO);
        }else if(perperty ==1){
            trapDao.addRectangle(trapVO);
        }
        return num;
    }

    /**
     * 修改电子围栏信息
     * @param trapVO
     * @return
     * @throws Exception
     */
    public int updateTrap(TrapVO trapVO) throws Exception{
        int num = 0;
        //围栏类型：0 圆圈；1 长方形；2多边形'
        int perperty = trapVO.getPerperty();
        int addId = trapVO.getId();
        trapDao.updateTrap(trapVO);
        if(perperty==0){
            trapDao.updateCircle(trapVO);
        }else if(perperty ==1){
            trapDao.updateRectangle(trapVO);
        }
        return num;

    }

    /**
     * 删除电子围栏信息
     * @param id
     * @param perperty  围栏类型：0 圆圈；1 长方形；2多边形'
     * @throws Exception
     */
    public void deleteTrapById(int id,int perperty) throws Exception {
        trapDao.deleteTrapById(id);
        if(perperty == 0) {
            // 删除圆形电子围栏
            trapDao.deleteCircleByTrapId(id);
        }else if(perperty == 1){
            //删除矩形电子围栏
            trapDao.deleteRectangleByTrapId(id);
        }
    }

    /**
     * 查询所有围栏信息
     * @param trapVO
     * @return
     * @throws Exception
     */
    public List<TrapVO> selectAllTrap(TrapVO trapVO) throws Exception{
        List<TrapVO> list = trapDao.selectAllTrap(trapVO);
        //获取了主表信息，根据perperty来查询圆形还是矩形的信息
        if(null!=list && list.size()>0){
            for(int i=0;i<list.size();i++){
                //perperty 围栏类型：0 圆圈；1 长方形；2多边形'
                int perperty = list.get(i).getPerperty();
                int id = list.get(i).getId();
                if(perperty == 0){
                    List<TrapVO> circleList = trapDao.selectCircleByTrapId(id);
                    if(null!=circleList && circleList.size()>0){
                        list.get(i).setCircleLat(circleList.get(0).getCircleLat());
                        list.get(i).setCircleLng(circleList.get(0).getCircleLng());
                        list.get(i).setCircleRadius(circleList.get(0).getCircleRadius());
                    }
                }else if(perperty == 1) {
                    List<TrapVO> rectangleList = trapDao.selectRectangleByTrapId(id);
                    if(null!=rectangleList && rectangleList.size()>0){
                        list.get(i).setLeftBottomLat(rectangleList.get(0).getLeftBottomLat());
                        list.get(i).setLeftBottomLng(rectangleList.get(0).getLeftBottomLng());
                        list.get(i).setLeftTopLat(rectangleList.get(0).getLeftTopLat());
                        list.get(i).setLeftTopLng(rectangleList.get(0).getLeftTopLng());
                        list.get(i).setRightBottomLat(rectangleList.get(0).getRightBottomLat());
                        list.get(i).setRightBottomLng(rectangleList.get(0).getRightBottomLng());
                        list.get(i).setRightTopLat(rectangleList.get(0).getRightTopLat());
                        list.get(i).setRightTopLng(rectangleList.get(0).getRightTopLng());
                    }
                }
            }
        }
        return list;
    }
}
