package com.car.service;

import com.car.util.CommUtil;
import com.car.vo.EquipmentVO;
import com.car.vo.PeopleVO;
import com.car.vo.TowerVO;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class ImportExcelService {
    @Autowired
    private EquipentService eqService;
    @Autowired
    private TowerService towerService;

    private String[] PersonheadFileds = {"姓名","所属部门","岗位","性别","手机号","设备编号","紧急联系人","紧急联系人手机"};
    private String[] EquipmentheadFileds = {"设备类型","设备型号","设备编号","SIM卡号","状态","开通时间","截止时间"};
    private String[] TowerheadFileds = {"杆塔名称","经度","纬度"};

    public String handleDataForXlsx(String name, String filename, InputStream inputStream) throws Exception{
        XSSFWorkbook xwb;
        XSSFSheet sheet;
        XSSFRow headRow;
        String code = "0";
        try {
            xwb = new XSSFWorkbook(inputStream);
            sheet = xwb.getSheetAt(0);
            headRow = sheet.getRow(0);
        } catch(Exception e) {
            code = "1";//"对不起,您的模板格式有误,请更换成最新的导入模板!"
            throw new Exception(code);
        }
        String[] headers = {};
        if("person".equalsIgnoreCase(name)){
            headers = this.PersonheadFileds;
        }else if("equipment".equalsIgnoreCase(name)){
            headers = this.EquipmentheadFileds;
        }else if("tower".equalsIgnoreCase(name)){
            headers = this.TowerheadFileds;
        }
        boolean isRight = this.checkTemplateIsRight(headRow,headers);
        if(!isRight) {
            code = "1";//"对不起,您的模板格式有误,请更换成最新的导入模板!"
            return code;
        }
        XSSFRow row;
        int count = 0;
        int rows = sheet.getPhysicalNumberOfRows();
        if(rows==0) return "1";
        if(rows==1) return "您所上传的文件没有数据!";
        for(int i = 1; i < rows; i++){
            row = sheet.getRow(i);
            //检测对应的模板
            if("person".equals(name)){

            }else if("equipment".equals(name)){
                //判断时期格式
                String bgTime = this.getCellValue(row,5);
                String edTime = this.getCellValue(row,6);
                if(bgTime!=null){
                    String bt = CommUtil.getExcelTime(Integer.parseInt(bgTime));
                    boolean isDate1 = CommUtil.isValidDate(bt);
                    if(!isDate1) {
                        code = "第"+(i+1)+"行开通时间格式【"+bgTime+"】不正确!";
                        break;
                    }
                }
                if(edTime!=null){
                    String ed = CommUtil.getExcelTime(Integer.parseInt(edTime));
                    boolean isDate2 = CommUtil.isValidDate(ed);
                    if(!isDate2) {
                        code = "第"+(i+1)+"行截止时间格式【"+edTime+"】不正确!";
                        break;
                    }
                }
                code = this.getEqDataFromExcelRow(row,i);
                if(!"0".equals(code)){
                    break;
                }
            }
            //导入杆塔信息，暂时用（后期将所有数据加进来）
            else if("tower".equals(name)){
                code = this.getTowerDataFormExcelRow(row,i);
            }
            count++;
        }
        return "{\"code\":\"" +code+"\",\"count\":\""+count+"\"}";
    }


    //检测导入模板是否有效(2010)
    private boolean checkTemplateIsRight(XSSFRow headRow, String[] headFileds) throws Exception {
        if(headRow == null) return false;
        for (int i = 0; i < headFileds.length; i++) {
            XSSFCell cell = headRow.getCell(i);
            if(cell == null) {
                return false;
            } else {
                String field = headFileds[i];
                if(!field.equals(cell.toString().trim())) {
                    return false;
                }
            }
        }

        return true;
    }

    //检测导入模板是否有效(2003-2007)
    private boolean checkTemplateIsRight(HSSFRow headRow, String[] headFileds) throws Exception {
        if(headRow == null) return false;
        for (int i = 0; i < headFileds.length; i++) {
            HSSFCell cell = headRow.getCell(i);
            if(cell == null) {
                return false;
            } else {
                String field = headFileds[i];
                if(!field.equals(cell.toString().trim())) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * 从XLSX中获取数据
     * @param row
     * @return
     * @throws Exception
     */
    private PeopleVO getPersonDataFromExcelRow(XSSFRow row) throws Exception {
        PeopleVO peopleVO = new PeopleVO();
        //姓名
        peopleVO.setName(this.getCellValue(row, 0));
        //部门
        peopleVO.setPosition(this.getCellValue(row, 1));



        return peopleVO;
    }
    private String getCellValue(XSSFRow row, int idx) {
        if(row.getCell(idx) != null) {
            row.getCell(idx).setCellType(XSSFCell.CELL_TYPE_STRING);
            return row.getCell(idx).toString().trim();
        }
        return null;
    }


    private String getPersonDataFromExcelRow(XSSFRow row,int i) throws Exception {
        String code = "0";
        PeopleVO peopleVO = new PeopleVO();
        //姓名
        String name =  this.getCellValue(row,0);
        //手机号
        String phone = this.getCellValue(row,4);
        //紧急联系人
        String relativeName = this.getCellValue(row,6);
        //紧急联系人手机
        String relativePhone = this.getCellValue(row,7);
        //性别
        String sex =  this.getCellValue(row,3);
        if(name==null){
            return "第"+(i+1)+"行姓名不能为空!";
        }
        if(phone==null){
            return "第"+(i+1)+"行手机号不能为空!";
        }
        if(relativeName==null){
            return "第"+(i+1)+"行紧急联系人不能为空!";
        }
        if(relativePhone==null){
            return "第"+(i+1)+"行紧急联系人手机不能为空!";
        }
        if(sex==null) sex = "1";
        peopleVO.setName(name);
        peopleVO.setMobile(phone);
        return code;
    }
    /**
     * 终端设备读取导入的Excel的信息  xlsx的
     * @param row
     * @param i
     * @return
     * @throws Exception
     */
    private String getEqDataFromExcelRow(XSSFRow row,int i) throws Exception {
        String code = "0";
        EquipmentVO eqVo = new EquipmentVO();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd ");
        String type = this.getCellValue(row,0);
        String sn = this.getCellValue(row,2);
        String sim = this.getCellValue(row,3);
        String status = this.getCellValue(row,4);
        if("定位终端".equalsIgnoreCase(type)){
            type = "1";
        }else if("高精度终端".equalsIgnoreCase(type)){
            type = "2";
        }
        String eqStatus = status =="在线"?"0": "1";//默认是离线
        String bgTime = this.getCellValue(row,5);
        String edTime = this.getCellValue(row,6);

        if(bgTime !=null ){
            String bt = CommUtil.getExcelTime(Integer.parseInt(bgTime));
            Date opDate = sdf.parse(bt);
            eqVo.setOpenDate(opDate);
        }

        if(edTime!=null){
            String ed = CommUtil.getExcelTime(Integer.parseInt(edTime));
            Date edDate = sdf.parse(ed);
            eqVo.setEndDate(edDate);
        }

        if(bgTime==null){
            return "第"+(i+1)+"行开通时间不能为空!";
        }
        if(edTime==null){
            return "第"+(i+1)+"行截止时间不能为空!";
        }
        if(sn==null) {
            return "第"+(i+1)+"行设备编号不能为空!";
        }
        eqVo.setType(Short.parseShort(type));
        eqVo.setModel(this.getCellValue(row,1));
        eqVo.setSn(this.getCellValue(row,2));
        eqVo.setSim(sim);
        eqVo.setStatus(Integer.parseInt(eqStatus));
        Date currTime = new Date();
        eqVo.setInDate(currTime);


        int num = eqService.addEquipment(eqVo);
        if(num ==1 ){
            //设备号已存在
            code = "第"+(i+1)+"行设备编号【"+eqVo.getSn()+"】已存在!";
        }else if(num ==  2) {
            //SIM卡号已存在
            code = "第"+(i+1)+"行SIM卡号【"+eqVo.getSim()+"】已存在!";
        }
        return code;
    }
    private String getEqDataFromExcelRow(HSSFRow row,int i) throws Exception {
        String code = "0";
        EquipmentVO eqVo = new EquipmentVO();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd ");
        String type = this.getCellValue(row,0);
        String sn = this.getCellValue(row,2);
        String sim = this.getCellValue(row,3);
        String status = this.getCellValue(row,4);
        if("定位终端".equalsIgnoreCase(type)){
            type = "1";
        }else if("高精度终端".equalsIgnoreCase(type)){
            type = "2";
        }
        String eqStatus = status =="在线"?"0": "1";//默认是离线
        String bgTime = this.getCellValue(row,5);
        String edTime = this.getCellValue(row,6);

        if(bgTime !=null ){
            Date opDate = sdf.parse(bgTime);
            eqVo.setOpenDate(opDate);
        }

        if(edTime!=null){
            Date edDate = sdf.parse(edTime);
            eqVo.setEndDate(edDate);
        }

        if(bgTime==null){
            return "第"+(i+1)+"行开通时间不能为空!";
        }
        if(edTime==null){
            return "第"+(i+1)+"行截止时间不能为空!";
        }
        if(sn==null) {
            return "第"+(i+1)+"行设备编号不能为空!";
        }
        eqVo.setType(Short.parseShort(type));
        eqVo.setModel(this.getCellValue(row,1));
        eqVo.setSn(this.getCellValue(row,2));
        eqVo.setSim(sim);
        eqVo.setStatus(Integer.parseInt(eqStatus));
        Date currTime = new Date();
        eqVo.setInDate(currTime);


        int num = eqService.addEquipment(eqVo);
        if(num ==1 ){
            //设备号已存在
            code = "第"+(i+1)+"行设备编号【"+eqVo.getSn()+"】已存在!";
        }else if(num ==  2) {
            //SIM卡号已存在
            code = "第"+(i+1)+"行SIM卡号【"+eqVo.getSim()+"】已存在!";
        }
        return code;
    }
    private String getCellValue(HSSFRow row, int idx) {
        if(row.getCell(idx) != null) {
            row.getCell(idx).setCellType(HSSFCell.CELL_TYPE_STRING);
            return row.getCell(idx).toString().trim();
        }
        return null;
    }

    /**
     * Excel 2007 以前的  xls
     * @param name
     * @param filename
     * @param inputStream
     * @return
     */
    public String handleDataForXls(String name, String filename, InputStream inputStream) throws Exception{
        String code = "0";
        HSSFWorkbook xwb;
        HSSFSheet sheet;
        HSSFRow headRow;
        try {
            xwb = new HSSFWorkbook(inputStream);
            sheet = xwb.getSheetAt(0);
            headRow = sheet.getRow(0);
        } catch(Exception e) {
            throw new Exception("对不起,您的模板格式有误,请更换成最新的导入模板!");
        }

        String[] headers = name=="person"?this.PersonheadFileds:
                this.EquipmentheadFileds;
        boolean isRight = this.checkTemplateIsRight(headRow,headers);
        if(!isRight) {
            code = "1";//"对不起,您的模板格式有误,请更换成最新的导入模板!"
            return code;
        }
        HSSFRow row;
        int count = 0;
        int rows = sheet.getPhysicalNumberOfRows();
        if(rows==0) return "1";
        if(rows==1) return "您所上传的文件没有数据!";
        for(int i = 1; i < rows; i++){
            row = sheet.getRow(i);
            //检测对应的模板
            if("person".equals(name)){

            }else if("equipment".equals(name)){
                //判断时期格式
                String bgTime = this.getCellValue(row,5);
                String edTime = this.getCellValue(row,6);
                if(bgTime!=null){
                    boolean isDate1 = CommUtil.isValidDate(bgTime);
                    if(!isDate1) {
                        code = "第"+(i+1)+"行开通时间格式【"+bgTime+"】不正确!";
                        break;
                    }
                }
                if(edTime!=null){
                    boolean isDate2 = CommUtil.isValidDate(edTime);
                    if(!isDate2) {
                        code = "第"+(i+1)+"行截止时间格式【"+edTime+"】不正确!";
                        break;
                    }
                }
                code = this.getEqDataFromExcelRow(row,i);
                if(!"0".equals(code)){
                    break;
                }
            }
            count++;
        }
        return "{\"code\":\"" +code+"\",\"count\":\""+count+"\"}";
    }

    /**
     * xlsx 格式过去杆塔Excel信息
     * @param row
     * @param i
     * @return
     */
    private String getTowerDataFormExcelRow(XSSFRow row, int i) throws Exception{
        String code = "0";
        TowerVO towerVO = new TowerVO();
        //姓名
        String name =  this.getCellValue(row,0);
        //经度
        Double lan = Double.parseDouble(this.getCellValue(row,1));
        //纬度
        Double lat = Double.parseDouble(this.getCellValue(row,2));

        if(name==null){
            return "第"+(i+1)+"行姓名不能为空!";
        }
        if(lat==null){
            return "第"+(i+1)+"行纬度不能为空!";
        }
        if(lan==null){
            return "第"+(i+1)+"行经度不能为空!";
        }
        towerVO.setVoltegeClass(Short.parseShort("2"));
        towerVO.setName(name);
        towerVO.setLat(lat);
        towerVO.setLan(lan);

        towerService.addTower(towerVO);

        return code;
    }
}
