package com.car.service;

import com.car.dao.IPeopleDao;
import com.car.dao.ITaskDao;
import com.car.vo.PeopleVO;
import com.car.vo.TaskVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeopleService {

    @Autowired
    private IPeopleDao peopleDao;
    @Autowired
    private ITaskDao taskDao;


    public List<PeopleVO> selectPersonByParams( PeopleVO pVo) throws Exception{
        List<PeopleVO> list = peopleDao.selectPersonByParams(pVo);
        if(list!=null && list.size()>0){
            String taskName = "";
            for(int i=0;i<list.size();i++){
                Integer pId = list.get(i).getId();
                List<TaskVO> taskList = taskDao.selectTaskByPId(pId);
                if(taskList!=null && taskList.size()>0){
                    for(int j=0;j<taskList.size();j++){
                        String tName = taskList.get(j).getName();
                        taskName += tName+";";
                    }
                    taskName = taskName.substring(0,taskName.length()-1);
                }
                list.get(i).setTaskNames(taskName);
            }
        }
        return list;
    }

    public int addPerson(PeopleVO pVo) throws Exception{
        int num = 0;
        //添加之前，先对手机进行查重
        PeopleVO pv = new PeopleVO();
        String phone = pVo.getMobile();
        pv.setMobile(phone);
        List<PeopleVO> list = peopleDao.selectPersonByParams(pv);
        if(null!=list && list.size()>0){
            num = 1;
        }else{
            peopleDao.addPerson(pVo);
            pVo.getId();
            if(pVo.getEquipmentId()!=null){
                peopleDao.insertPersonToEquipment(pVo);
            }
        }
        return num;
    }

    public int updatePerson(PeopleVO pVo) throws Exception{
        int num = 0;
        //判断手机号是否存在
        PeopleVO pv = new PeopleVO();
        //获取将要修改的手机号
        String phone = pVo.getMobile();
        Integer pId = pVo.getId();

        pv.setMobile(phone);
        List<PeopleVO> list = peopleDao.selectPersonByParams(pv);
        if(null!=list && list.size()>0 && list.get(0).getId() != pId){
            //存在手机信息，继续判断是否就是当前用户自己所有,如果ID不相同，则不能修改，提示手机号已存在
                num = 1;
        }else{
            peopleDao.updatePerson(pVo);
            if(pVo.getPersonToEquipmentId()==null && pVo.getEquipmentId()!=null){
                peopleDao.insertPersonToEquipment(pVo);
            }else if(pVo.getPersonToEquipmentId()!=null && pVo.getEquipmentId()==null){
                peopleDao.deletePersonToEqByPid(pVo.getId());
            }else if(pVo.getPersonToEquipmentId()!=null && pVo.getEquipmentId()!=null){
                peopleDao.updatePersonToEquipment(pVo);
            }
        }
        return num;
    }

    public void deletePersonById(int pId) throws Exception{
        peopleDao.deletePersonById(pId);
    }

    public void deletePersonToEqById(int pToEqId) throws Exception{
        peopleDao.deletePersonToEquipment(pToEqId);
    }
}
