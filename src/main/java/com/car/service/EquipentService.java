package com.car.service;

import com.car.dao.IEquipmentDao;
import com.car.vo.EquipmentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/10/21 0021.
 */
@Service
public class EquipentService {
    @Autowired
    private IEquipmentDao eqDao;

    public List<EquipmentVO> selectEquipmentByParmas(EquipmentVO eq) throws Exception{
        List<EquipmentVO> list = eqDao.selectEquipmentByParmas(eq);
        return list;
    }

    public int addEquipment(EquipmentVO eqVo) throws Exception{
        int num = 0;
        //判断设备号和SIM卡号是否重复
        EquipmentVO ev = new EquipmentVO();
        String sn = eqVo.getSn();//设备号
        String sim = eqVo.getSim();
        ev.setSn(sn);
        List<EquipmentVO> snList = eqDao.selectEquipmentByParmas(ev);
        //设备号已存在
        if(null!=snList && snList.size()>0){
            num = 1;
            return num;
        }
        ev.setSn(null);
        ev.setSim(sim);
        //SIM卡号已存在
        List<EquipmentVO> simList = eqDao.selectEquipmentByParmas(ev);
        if(null!=simList && simList.size()>0){
            num = 2;
            return num;
        }
        eqDao.addEquipment(eqVo);
        return num;
    }

    public int updateEquipment(EquipmentVO eq) throws Exception{
        int num = 0;
        int eqId = eq.getId();
        //修改之前，先判断设备号和SIM卡号是否存在
        EquipmentVO eqVo = new EquipmentVO();
        String sn = eq.getSn();//设备No
        String sim = eq.getSim();//设备SIM卡号

        eqVo.setSn(sn);
        List<EquipmentVO> snList = eqDao.selectEquipmentByParmas(eqVo);
        //设备号已存在
        if(null!=snList && snList.size()>0 && snList.get(0).getId()!=eqId){
            num = 1;
            return num;
        }
        eqVo.setSn(null);
        eqVo.setSim(sim);
        List<EquipmentVO> simList = eqDao.selectEquipmentByParmas(eqVo);
        //SIM卡号已存在
        if(null!=simList && simList.size()>0 && snList.get(0).getId()!=eqId){
            num = 2;
            return num;
        }
        eqDao.updateEquipment(eq);
        return num;
    }

    public void deleteEquipmentById(int eqId) throws Exception{
        eqDao.deleteEquipmentById(eqId);
    }

    public void deletePersonToEquipmentById(int eqId) throws Exception{
        eqDao.deletePersonToEquipmentById(eqId);
    }

    /**
     * 查询所有未被绑定的终端
     * @param eq
     * @return
     * @throws Exception
     */
    public List<EquipmentVO> selectNewEquipment(EquipmentVO eq) throws Exception{
        List<EquipmentVO> list = eqDao.selectNewEquipment(eq);
        return list;
    }
}
