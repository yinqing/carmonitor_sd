package com.car.service;

import com.car.dao.IPeopleDao;
import com.car.dao.ITaskDao;
import com.car.dao.ITowerDao;
import com.car.util.CommUtil;
import com.car.vo.PeopleVO;
import com.car.vo.TaskVO;
import com.car.vo.TowerVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * 作业管理
 */
@Service
public class TaskService {
    @Autowired
    private ITaskDao dao;
    @Autowired
    private ITowerDao towerDao;
    @Autowired
    private IPeopleDao peopleDao;

    /**
     * 查询所有作业管理信息
     * @param taskVO
     * @return
     * @throws Exception
     */
    public List<TaskVO> selectAllTask(TaskVO taskVO) throws Exception{
        List<TaskVO> list = dao.selectAllTask(taskVO);
        if(list!=null && list.size()>0){
            for(int i=0;i<list.size();i++){
                //获取作业Id
                int taskId = list.get(i).getId();
                //根据作业ID去查询相关的线路信息以及人员信息
                List<TowerVO> towerList = towerDao.selectTowerByTaskInfo(taskId);
                towerList.removeAll(Collections.singleton(null));
                String towerNames = "";
                String towerIds = "";
                if(towerList!=null && towerList.size()>0){
                    for(int k=0;k<towerList.size();k++){
                        String tName = towerList.get(k).getName();
                        Integer id = towerList.get(k).getId();
                        towerNames += tName +";";
                        towerIds += id + ";";

                    }
                    towerNames = towerNames.substring(0,towerNames.length()-1);
                    towerIds = towerIds.substring(0,towerIds.length()-1);
                }
                List<PeopleVO> peopleList = peopleDao.selectPersonByTaskInfo(taskId);
                peopleList.removeAll(Collections.singleton(null));
                String peopleNames = "";
                String peopleIds = "";
                if(peopleList!=null && peopleList.size()>0){
                    for(int j=0;j<peopleList.size();j++){
                        String pName = peopleList.get(j).getName();
                        Integer pId = peopleList.get(j).getId();
                        peopleNames += pName +";";
                        peopleIds += pId +";";
                    }
                    peopleNames = peopleNames.substring(0,peopleNames.length()-1);
                    peopleIds = peopleIds.substring(0,peopleIds.length()-1);
                }

                list.get(i).setTowerNames(towerNames);
                list.get(i).setPeopleNames(peopleNames);
                list.get(i).setTowerIds(towerIds);
                list.get(i).setPeopleIds(peopleIds);
            }
        }
        return list ;
    }

    /**
     * 新增作业管理，如果有作业名，则判断是否重复，如果没有，则自动生成
     * @param taskVO
     * @return
     * @throws Exception
     */
    public String addTask(TaskVO taskVO) throws Exception{
        TaskVO newVo = new TaskVO();
        String code = "0";
        String addName = taskVO.getName();//作业名称
        //如果前台没有传名称过来，则自动生成，如果传过来了，则检测是否唯一
        if(addName!=null && !addName.equals("")) {
            newVo.setName(addName);
            List<TaskVO> list = dao.selectAllTaskByParams(newVo);
            if(list!=null && list.size()>0){
                code = "1";//作业名称重复
                return code;
            }
        }else{
            String routeName = taskVO.getRouteName();//线路名称
            String currTime = CommUtil.getCurrentSysTimeToString("yyyy-MM-dd");
            Integer id = dao.selectLastTaskId();
            //自动创建name
            for(int i=1;i<10000;i++){
                if(id==null) id = i;
                else id += 1;
                String name = routeName+" "+currTime+" "+id;
                //将自动生成的Name再一次查重
                newVo.setName(name);
                List<TaskVO> list = dao.selectAllTaskByParams(newVo);
                if(list!=null && list.size()>0){
                    continue;
                }
                taskVO.setName(name);
                break;
            }
        }
        dao.addTask(taskVO);
        int addId = taskVO.getId();
        //新增后，再去关联表task_
        String towerIds = taskVO.getTowerIds();
        String poepleIds = taskVO.getPeopleIds();
        if(towerIds!=null){
            String [] tIds = towerIds.split(",");
            for(String tid:tIds){
                HashMap<String,String> map = new HashMap();
                map.put("towerId",tid);
                map.put("taskId",String.valueOf(addId));
                dao.addTaskTower(map);
            }
        }
        if(poepleIds!=null){
            String [] pIds = poepleIds.split(",");
            for(String pid:pIds){
                HashMap<String,String> map = new HashMap();
                map.put("peopleId",pid);
                map.put("taskId",String.valueOf(addId));
                dao.addTaskPeople(map);
            }
        }
        return code;
    }

    /**
     * 修改作业管理  主要是对作业名称查重
     * @param taskVO
     * @return
     * @throws Exception
     */
    public String updateTask(TaskVO taskVO) throws Exception{
        String code = "0";
        String name = taskVO.getName();
        Integer taskId = taskVO.getId();
        TaskVO newVo = new TaskVO();
        newVo.setName(name);
        List<TaskVO> list = dao.selectAllTaskByParams(newVo);
        if(list!=null && list.size()>0 && !taskId.equals(list.get(0).getId())){
            code = "1";//作业名称重复
            return code;
        }
        dao.updateTask(taskVO);
        dao.deleteTaskTowerById(taskId);
        dao.deleteTaskPeopleById(taskId);
        //删除关联关系后，再去新增关系表
        String towerIds = taskVO.getTowerIds();
        String poepleIds = taskVO.getPeopleIds();
        if(towerIds!=null){
            String [] tIds = towerIds.split(",");
            for(String tid:tIds){
                HashMap<String,String> map = new HashMap();
                map.put("towerId",tid);
                map.put("taskId",String.valueOf(taskId));
                dao.addTaskTower(map);
            }
        }
        if(poepleIds!=null){
            String [] pIds = poepleIds.split(",");
            for(String pid:pIds){
                HashMap<String,String> map = new HashMap();
                map.put("peopleId",pid);
                map.put("taskId",String.valueOf(taskId));
                dao.addTaskPeople(map);
            }
        }

        return code;
    }

    public void deleteTaskById(int id) throws Exception{
        dao.deleteTaskById(id);
        dao.deleteTaskPeopleById(id);
        dao.deleteTaskTowerById(id);
    }
}
