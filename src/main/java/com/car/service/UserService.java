package com.car.service;

import com.car.util.CommUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.car.bean.User;
import com.car.dao.IUserDao;

import java.util.List;

@Service
public class UserService {
	@Autowired
	private IUserDao userDao;

	public User findUserByNameAndPassword(String userName,String password) throws Exception {
		User user = null;
		user = userDao.findUserByNameAndPassword(userName,password);
		
		return user;
	}


	public int addUser(User user) throws Exception{
		int num = 0;
		//先查询该用户是否存在
		List<User> list = userDao.selectOnlyByParam(user);
		if(null!=list && list.size()>0){
			num = 1;//用户名已存在，不能添加
		}else{
			//密码进行MD5码加密 默认是123456
			String defaulPwd = "123456";
			String pwd = CommUtil.generateMD5Str(defaulPwd);
			user.setPasswordDecode(user.getPassword());
			user.setPassword(pwd);
			userDao.insertUser(user);
		}
		return num;
	}

	public int updateUser(User user) throws Exception {
		int num = 0;
		int id = user.getId();
		//先查询该用户是否存在
		List<User> list = userDao.selectOnlyByParam(user);
		if(null!=list && list.size()>0 && list.get(0).getId()!=id){
			num = 1;//用户名已存在，修改添加
		}else{
			userDao.updateUser(user);
		}
		return num;
	}

	public void deleteUserById(String[] ids) throws Exception{
		if(null!=ids && ids.length>0){
			for(String uid:ids){
				userDao.deleteUserById(uid);
			}
		}
	}

	public List<User> selectUserByParam(User user) throws Exception{
		List<User> list = userDao.selectUserByParam(user);
		return list;
	}

	public int updatetPassword(String id, String oldPwd, String newPwd) throws Exception{
		int code = 0;
		User user = userDao.selectUserById(id);
		String pwd = user.getPassword();
		if(!pwd.equals(CommUtil.generateMD5Str(oldPwd))){
			//原密码不正确
			code = 1;
		}else{
			User u = new User();
			u.setId(Integer.parseInt(id));
			u.setPasswordDecode(newPwd);
			u.setPassword(CommUtil.generateMD5Str(newPwd));
			userDao.updateUser(u);
		}
		return code;
	}
}
