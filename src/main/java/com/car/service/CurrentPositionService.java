package com.car.service;

import com.car.dao.ICurrentPositionDao;
import com.car.vo.CurrentPositionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 实时位置
 * Created by yinqing on 2017/10/25.
 */
@Service
public class CurrentPositionService {

    @Autowired
    private ICurrentPositionDao dao;

    public List<CurrentPositionVO> selectCurrPosition(CurrentPositionVO currVo) throws Exception {

        return dao.selectCurrPosition(currVo);
    }
}
