package com.car.bean;

import java.util.Date;

public class Equipment {
    private Integer id;//id

    private Integer status;//状态（0:在线、1:离线）

    private Short type;//终端类型

    private String model;//设备型号

    private String sn;//终端编号

    private String sim;//SIM卡

    private Integer peopelId;//人员Id

    private Date inDate;//入库时间

    private Date endDate;//sim截止时间

    private Date openDate; //sim卡开通时间

    public Equipment(Integer id, Integer status, Short type,String model, String sn, String sim, Integer peopelId, Date inDate, Date endDate, Date openDate) {
        this.id = id;
        this.status = status;
        this.type = type;
        this.model = model;
        this.sn = sn;
        this.sim = sim;
        this.peopelId = peopelId;
        this.inDate = inDate;
        this.endDate = endDate;
        this.openDate = openDate;
    }

    public Equipment() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn == null ? null : sn.trim();
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim == null ? null : sim.trim();
    }

    public Integer getPeopelId() {
        return peopelId;
    }

    public void setPeopelId(Integer peopelId) {
        this.peopelId = peopelId;
    }

    public Date getInDate() {
        return inDate;
    }

    public void setInDate(Date inDate) {
        this.inDate = inDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}