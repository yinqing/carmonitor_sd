package com.car.bean;

public class LoginUser {
	private String userId;
	private String userName;
	private String realName;
	private String nickName;
	private String userType; //用户类型 0=驾校  1=超级管理员账户  2=合作伙伴
	private String authOrgId; //权限机构id (数据权限)
	private String authOrg;
	private String orgId;
	private String orgName;
	private String ip;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getAuthOrgId() {
		return authOrgId;
	}
	public void setAuthOrgId(String authOrgId) {
		this.authOrgId = authOrgId;
	}
	public String getAuthOrg() {
		return authOrg;
	}
	public void setAuthOrg(String authOrg) {
		this.authOrg = authOrg;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
}
