package com.car.bean;

public class People {
    private Integer id;

    private String name;

    private String position;

    private String mobile;

    private String relativeName;

    private String relativeMobile;

    private Integer classId;

    private Integer departmentId;

    private Integer companyId;

    private String sex;//性别

    private Integer equipmentId;//设备终端ID

    private String photoImg;//头像名称

    public People(Integer id, String name, String position, String mobile, String relativeName, String relativeMobile, Integer classId, Integer departmentId, Integer companyId, String sex, Integer equipmentId) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.mobile = mobile;
        this.relativeName = relativeName;
        this.relativeMobile = relativeMobile;
        this.classId = classId;
        this.departmentId = departmentId;
        this.companyId = companyId;
        this.sex = sex;
        this.equipmentId = equipmentId;
    }

    public People() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getRelativeName() {
        return relativeName;
    }

    public void setRelativeName(String relativeName) {
        this.relativeName = relativeName == null ? null : relativeName.trim();
    }

    public String getRelativeMobile() {
        return relativeMobile;
    }

    public void setRelativeMobile(String relativeMobile) {
        this.relativeMobile = relativeMobile == null ? null : relativeMobile.trim();
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Integer equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getPhotoImg() {
        return photoImg;
    }

    public void setPhotoImg(String photoImg) {
        this.photoImg = photoImg;
    }
}