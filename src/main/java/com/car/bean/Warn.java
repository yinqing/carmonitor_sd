package com.car.bean;


import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 报警信息
 * yinqing 20127-11-20
 */
public class Warn {
    private Integer id;//主键

    private String eqNum;//设备终端号码

    private String status;//设备状态，0：未处理，1：已处理

    private String num;//编号

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date time;//时间

    private Double lat;//纬度

    private Double lng;//经度

    private String type;//类型

    private String content;//报警内容

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEqNum() {
        return eqNum;
    }

    public void setEqNum(String eqNum) {
        this.eqNum = eqNum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
