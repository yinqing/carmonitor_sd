package com.car.bean;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 任务\作业管理
 * yinqing
 * 2017-12-14
 */
public class Task {
    private Integer id;//主键

    private String name;//作业名称

    private String type;//作业类型 0：月度，1：日常

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginTime;//作业开始时间

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;//作业截止时间

    private String monthBgtime;//作业开始时间(月度作业)

    private String monthEdtime;//作业截止时间（月度作业）

    private Integer routeId;//线路ID

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getMonthBgtime() {
        return monthBgtime;
    }

    public void setMonthBgtime(String monthBgtime) {
        this.monthBgtime = monthBgtime;
    }

    public String getMonthEdtime() {
        return monthEdtime;
    }

    public void setMonthEdtime(String monthEdtime) {
        this.monthEdtime = monthEdtime;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }
}
