package com.car.vo;

import java.util.Date;
import java.util.List;

/**
 * 实时位置
 * Created by yinqing on 2017/10/25.
 */
public class CurrentPositionVO {
    private Integer id;//主键

    private String SIMH;//终端设备号

    private Double DWJD;//经度

    private Double DWWD;//纬度

    private String CLZT;//状态

    private Float CLSD;//速度

    private Float CLFX;//方向

    private Date GPST;//GPS时间

    private Date RQSJ;//入库时间

    private String orgId;

    private String carId;

    private String plateNumber;

    private Float temperature;

    private String peopleName;

    private String mobile;

    private String routeName;

    private Integer peopleId;

    private Integer routeId;

    private String taskName;//作业名称
    private Integer taskId ;//作业ID

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSIMH() {
        return SIMH;
    }

    public void setSIMH(String SIMH) {
        this.SIMH = SIMH;
    }

    public Double getDWJD() {
        return DWJD;
    }

    public void setDWJD(Double DWJD) {
        this.DWJD = DWJD;
    }

    public Double getDWWD() {
        return DWWD;
    }

    public void setDWWD(Double DWWD) {
        this.DWWD = DWWD;
    }

    public String getCLZT() {
        return CLZT;
    }

    public void setCLZT(String CLZT) {
        this.CLZT = CLZT;
    }

    public Float getCLSD() {
        return CLSD;
    }

    public void setCLSD(Float CLSD) {
        this.CLSD = CLSD;
    }

    public Float getCLFX() {
        return CLFX;
    }

    public void setCLFX(Float CLFX) {
        this.CLFX = CLFX;
    }

    public Date getGPST() {
        return GPST;
    }

    public void setGPST(Date GPST) {
        this.GPST = GPST;
    }

    public Date getRQSJ() {
        return RQSJ;
    }

    public void setRQSJ(Date RQSJ) {
        this.RQSJ = RQSJ;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    public String getPeopleName() {
        return peopleName;
    }

    public void setPeopleName(String peopleName) {
        this.peopleName = peopleName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public Integer getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(Integer peopleId) {
        this.peopleId = peopleId;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }
}
