package com.car.vo;

import java.util.Date;

/**
 * 报警信息
 * yinqing 2017-11-20
 */
public class WarnVO {
    private Integer id;//主键

    private String eqNum;//设备终端号码

    private String status;//设备状态，0：未处理，1：已处理

    private String num;//编号

    private Date time;//时间

    private Double lat;//纬度

    private Double lng;//经度

    private String type;//类型

    private String content;//报警内容

    private String person;//报警人

    private String phone ;//报警人电话

    private String personImg;//报警人头像

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEqNum() {
        return eqNum;
    }

    public void setEqNum(String eqNum) {
        this.eqNum = eqNum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPersonImg() {
        return personImg;
    }

    public void setPersonImg(String personImg) {
        this.personImg = personImg;
    }
}
