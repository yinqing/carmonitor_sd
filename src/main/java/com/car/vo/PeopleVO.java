package com.car.vo;

public class PeopleVO {
    private Integer id;

    private String name;

    private String position;

    private String mobile;

    private String relativeName;

    private String relativeMobile;

    private Integer classId;

    private Integer departmentId;

    private Integer companyId;

    private String sex;//性别

    private Integer equipmentId;//设备终端ID

    private String equipmentNo;//设备终端号

    private String company;//公司名称

    private String department;//部门名称

    private String className;//班级名称

    private Integer personToEquipmentId;//人员和设备关联ID

    private String photoImg;//头像名称
    private String taskNames;//作业名称
    private String warnInfo;//预警内容
    public PeopleVO(Integer id, String name, String position,
                    String mobile, String relativeName, String relativeMobile,
                    Integer classId, Integer departmentId, Integer companyId,
                    String sex, Integer equipmentId,String company, String department,String className,String equipmentNo,Integer personToEquipmentId) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.mobile = mobile;
        this.relativeName = relativeName;
        this.relativeMobile = relativeMobile;
        this.classId = classId;
        this.departmentId = departmentId;
        this.companyId = companyId;
        this.sex = sex;
        this.equipmentId = equipmentId;
        this.company = company;
        this.department = department;
        this.className = className;
        this.equipmentNo = equipmentNo;
        this.personToEquipmentId = personToEquipmentId;
    }

    public PeopleVO() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getRelativeName() {
        return relativeName;
    }

    public void setRelativeName(String relativeName) {
        this.relativeName = relativeName == null ? null : relativeName.trim();
    }

    public String getRelativeMobile() {
        return relativeMobile;
    }

    public void setRelativeMobile(String relativeMobile) {
        this.relativeMobile = relativeMobile == null ? null : relativeMobile.trim();
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Integer equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getEquipmentNo() {
        return equipmentNo;
    }

    public void setEquipmentNo(String equipmentNo) {
        this.equipmentNo = equipmentNo;
    }

    public Integer getPersonToEquipmentId() {
        return personToEquipmentId;
    }

    public void setPersonToEquipmentId(Integer personToEquipmentId) {
        this.personToEquipmentId = personToEquipmentId;
    }

    public String getPhotoImg() {
        return photoImg;
    }

    public void setPhotoImg(String photoImg) {
        this.photoImg = photoImg;
    }

    public String getTaskNames() {
        return taskNames;
    }

    public void setTaskNames(String taskNames) {
        this.taskNames = taskNames;
    }

    public String getWarnInfo() {
        return warnInfo;
    }

    public void setWarnInfo(String warnInfo) {
        this.warnInfo = warnInfo;
    }
}