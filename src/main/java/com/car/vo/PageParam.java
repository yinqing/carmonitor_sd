package com.car.vo;


import com.car.util.CommUtil;

/**
 * 
 * @author yinqing
 * @date 2017-10-25
 * @version 1.0
 * @description 分页查询参数封装类
 *
 */
public class PageParam {
	public static final String DEF_PAGE_SIZE = "15";
	
	private String start; //接收前台传递的页号信息
	private String limit; //接收前台传递的页大小信息
	private int pageNum; //请求的页号
	private int pageSize; //请求的页大小
	
	public PageParam() {
		this.start = "0"	;
		this.limit = PageParam.DEF_PAGE_SIZE;
		this.pageNum = 1;
		this.pageSize = Integer.parseInt(this.limit);
	}
	
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		if(CommUtil.isEmptyStr(start)) {
			start = "0";
		}
		this.start = start;
		this.setPageNum(Integer.parseInt(start)/Integer.parseInt(this.limit) + 1);
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		if(CommUtil.isEmptyStr(limit)) {
			limit = PageParam.DEF_PAGE_SIZE;
		}
		this.limit = limit;
		this.setPageSize(Integer.parseInt(limit));
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
}
