package com.car.filter;

import com.car.bean.LoginUser;
import com.car.util.CommUtil;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by yinqing on 2017/10/26 0026.
 */
public class LoginFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        String path = request.getContextPath();
        String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
        String uri = request.getRequestURI();
        String reqType = request.getHeader("x-requested-with");
        LoginUser loginUser = CommUtil.getLoginUserFromSession(request);
        if(uri.lastIndexOf(".jsp")>0 &&loginUser==null &&
                uri.lastIndexOf("login.jsp")<0  && uri.lastIndexOf("show.html")<0) {
            String url = basePath + "/index.do";
            response.sendRedirect(url);
            return;

        } else if(loginUser==null && uri.lastIndexOf("index.do")<0 && uri.lastIndexOf("login.do")<0 &&  uri.lastIndexOf(".do")>0){
            response.getWriter().write("{\"success\":false,\"message\":\"unlogin\"}");
            return;
        }
        //排除以下请求,即对它们不做登录验证,直接放行
//        else if(loginUser == null && uri.lastIndexOf(".do")>0 && uri.lastIndexOf("/index.do")<0
//                && uri.lastIndexOf("/login.do")<0 && uri.lastIndexOf("/findBaseStationByParam.do")<0
//                && uri.lastIndexOf("chromeDownload.do")<0 && uri.lastIndexOf("firefoxDownload.do")<0) {
//            if("XMLHttpRequest".equals(reqType)) {
//                response.getWriter().write("{\"success\":\"false\",\"message\":\"unlogin\"}");
//            } else {
//                String url = basePath + "/index.do";
//                response.sendRedirect(url);
//                return;
//            }
//        }
        else {
            chain.doFilter(req, resp);
        }
    }

    public void destroy() {
    }
}
