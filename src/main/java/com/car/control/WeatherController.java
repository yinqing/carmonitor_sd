package com.car.control;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by yinqing on 2017/11/17.
 */
@Controller
public class WeatherController {

    /**
     * 获取外网IP
     * @return
     * @throws Exception
     */
    public static String getIp() throws Exception{
        //参数字符串，如果拼接在请求链接之后，需要对中文进行 URLEncode   字符集 UTF-8
        StringBuilder sb = new StringBuilder();
        InputStream    is=null;
        BufferedReader br=null;
        PrintWriter out = null;
        String ip = "";
        try {
            //接口地址
            String            url        = "http://ip.chinaz.com/getip.aspx";
            URL               uri        = new URL(url);
            HttpURLConnection connection= (HttpURLConnection) uri.openConnection();
            connection.setRequestMethod("POST");
            connection.setReadTimeout(5000);
            connection.setConnectTimeout(10000);
            connection.setRequestProperty("accept", "*/*");
            //发送参数
            connection.setDoOutput(true);
            out = new PrintWriter(connection.getOutputStream());
            out.flush();
            //接收结果
            is = connection.getInputStream();
            br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String         line;
            //缓冲逐行读取
            while ( (line = br.readLine()) != null ) {
                sb.append(line);
            }
        }catch ( Exception ignored ){}
        finally {
            //关闭流
            try {
                if(is!=null){
                    is.close();
                }
                if(br!=null){
                    br.close();
                }
                if (out!=null){
                    out.close();
                }
                //输出结果
                ip = sb.toString();
            } catch (IOException e2) {}
        }
        return ip;
    }

    /**
     * 获取天气信息 每天只能查询1000次（免费的）
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/getWeatherInfo.do",method = {RequestMethod.POST,RequestMethod.GET})
    public void getWeatherInfo(HttpServletRequest request,HttpServletResponse response) throws Exception {
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        //参数字符串，如果拼接在请求链接之后，需要对中文进行 URLEncode   字符集 UTF-8
        String localIp = this.getIp();
        JSONObject jIp = JSON.parseObject(localIp);
        String ip = jIp.getString("ip");
        if(null==ip ||"".equals(ip)){
            ip = "CN101091001";//这是邯郸的城市代码
        }
        String param = "key=b2752f0953ee404e888d1865f87a4674&location="+ip;
        StringBuilder sb = new StringBuilder();
        InputStream    is=null;
        BufferedReader br=null;
        PrintWriter out = null;
        try {
            //接口地址
            String            url        = "https://free-api.heweather.com/s6/weather";
//            String            url        = "https://api.heweather.com/s6/weather";
            URL               uri        = new URL(url);
            HttpURLConnection connection= (HttpURLConnection) uri.openConnection();
            connection.setRequestMethod("POST");
            connection.setReadTimeout(5000);
            connection.setConnectTimeout(10000);
            connection.setRequestProperty("accept", "*/*");
            //发送参数
            connection.setDoOutput(true);
            out = new PrintWriter(connection.getOutputStream());
            out.print(param);
            out.flush();
            //接收结果
            is = connection.getInputStream();
            br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
//            br = new BufferedReader(new InputStreamReader(is));
            String         line;
            //缓冲逐行读取
            while ( (line = br.readLine()) != null ) {
                sb.append(line);
            }
        }catch ( Exception ignored ){}
        finally {
            //输出结果
            out.println(sb.toString());
            //关闭流
            try {
                if(is!=null){
                    is.close();
                }
                if(br!=null){
                    br.close();
                }
                if (out!=null){
                    out.close();
                }
            response.getWriter().write(sb.toString());
            } catch (IOException e2) {}
        }
    }
}
