package com.car.control;

import com.alibaba.fastjson.JSON;
import com.car.service.TaskService;
import com.car.vo.TaskVO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 作业管理
 * yinqing  2017-12-14
 */
@Controller
public class TaskController {
    @Autowired
    private TaskService service;
    private static Logger logger  = Logger.getLogger(TaskController.class);

    /**
     * 查询所有作业管理信息
     * @param taskVO
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/selectAllTask.do",method = RequestMethod.POST)
    public void selectAllTask(TaskVO taskVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        String msg = "";
        try{
           List<TaskVO> list =  service.selectAllTask(taskVO);
            json = JSON.toJSON(list).toString();
            json = "{\"success\":true,\"dataCount\":"+list.size()+",\"data\":"+json+"}";
            logger.info("查询所有作业管理总数======"+list.size());
        }catch (Exception e){
            e.printStackTrace();
            logger.error("查询作业管理异常===="+e);
            msg = "系统繁忙，请稍后再试";
            json = "{\"success\":false,\"message\":\""+msg+"\"}";
        }

        response.getWriter().write(json);
    }

    /**
     * 新增作业管理
     * @param taskVO
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/addTask.do",method = RequestMethod.POST)
    public void addTask(TaskVO taskVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        String msg = "";
        try{
            String code = service.addTask(taskVO);
            if("0".equalsIgnoreCase(code)){
                msg = "新增成功";
                json = "{\"success\":true,\"message\":\""+msg+"\"}";
            }else if("1".equalsIgnoreCase(code)){
                msg = "作业名已存在，请重新填写!";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("新增作业管理异常===="+e);
            msg = "系统繁忙，请稍后再试";
            json = "{\"success\":false,\"message\":\""+msg+"\"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 修改作业管理信息
     * @param taskVO
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/updateTask.do",method = RequestMethod.POST)
    public void updateTask(TaskVO taskVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        String msg = "";
        try{
            String code = service.updateTask(taskVO);
            if("0".equalsIgnoreCase(code)){
                msg = "修改成功";
                json = "{\"success\":true,\"message\":\""+msg+"\"}";
            }else if("1".equalsIgnoreCase(code)){
                msg = "作业名已存在，请重新填写!";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("修改作业管理异常===="+e);
            msg = "系统繁忙，请稍后再试";
            json = "{\"success\":false,\"message\":\""+msg+"\"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 批量删除作业管理
     * @param ids
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/deleteTaskById.do",method = RequestMethod.POST)
    public void deleteTaskById(String ids, HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        String msg = "";
        try{
            String [] idArr = ids.split(",");
            for(String taskId :idArr){
                int id = Integer.parseInt(taskId);
                service.deleteTaskById(id);
            }

        }catch (Exception e){
            e.printStackTrace();
            logger.error("删除作业管理异常===="+e);
            msg = "系统繁忙，请稍后再试";
            json = "{\"success\":false,\"message\":\""+msg+"\"}";
        }
        response.getWriter().write(json);
    }
}
