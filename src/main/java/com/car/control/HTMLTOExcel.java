package com.car.control;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@Controller
public class HTMLTOExcel {
    /**
     *
     * @param html 页面html字符串
     * @param sheetName sheet页名称
     * @param fileName 下载的文件名
     * @param type  如果是full 那么将第一列和第二列隐藏，第一列是序号，第二列是复选框
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/tableToExcel.do",method = {RequestMethod.POST,RequestMethod.GET})
    //toExcel是根据html文件地址生成对应的xls
    public void toExcel(String html, String sheetName, String fileName, String type, HttpServletRequest request, HttpServletResponse response)throws IOException{
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        OutputStream os =null;

        Document doc = Jsoup.parse(html);
        String title = doc.title();
        ///得到样式，以后可以根据正则表达式解析css，暂且没有找到cssparse
        Elements style= doc.getElementsByTag("style");
        ///得到Table，demo只演示输入一个table，以后可以用循环遍历tables集合输入所有table
        Elements tables= doc.getElementsByTag("TABLE");
        if(tables.size()==0){
            return;
        }
        Element table=tables.get(0);
        //得到所有行
        Elements trs = table.getElementsByTag("tr");
        ///得到列宽集合
        Elements colgroups=table.getElementsByTag("style");
        try {
            //设置列名
            Element hd = trs.get(0);
            String hdText = hd.text().trim();
            String[] headers = hdText.split(" ");
            String [] columns = new String [trs.size()];

//            List list
            for(int i=1;i<trs.size();i++){
                Element tr = trs.get(i);
                Elements tds = tr.getElementsByTag("td");
                String text = "";
                for(int j=0;j<tds.size();j++){
                    text += tds.get(j).text()+"@";
                }
                columns[i] = text.substring(0,text.length()-1);
            }
            Workbook wb =  createWorkBook(headers,columns,sheetName,type);
            String fName = "下载.xls";
            if(null!=fileName && ""!=fileName){
                fName = fileName;
            }
            response.setContentType("APPLICATION/OCTET-STREAM");
//            response.setHeader("Content-Disposition",  "attachment; filename="
//                    + URLEncoder.encode(fName+".xls", "UTF-8"));

            response.addHeader("Content-Disposition", "attachment;filename="+ new String((fName).getBytes("GB2312"),"ISO-8859-1"));

            os = response.getOutputStream();
            wb.write(os);
            os.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Workbook createWorkBook(String columnNames[],String columnValues[],String sheetName,String type) {
        // 创建excel工作簿
        Workbook wb = new HSSFWorkbook();
        // 创建第一个sheet（页），并命名
        String shName = "sheet";
        if(null!=sheetName && ""!=sheetName){
            shName = sheetName;
        }
        Sheet sheet = wb.createSheet(shName);

        // 创建第一行
        Row row = sheet.createRow((short) 0);
        // 创建两种单元格格式
        CellStyle cs = wb.createCellStyle();
        CellStyle cs2 = wb.createCellStyle();

        // 创建两种字体
        Font f = wb.createFont();
        Font f2 = wb.createFont();

        // 创建第一种字体样式（用于列名）
        f.setFontHeightInPoints((short) 10);
        f.setColor(IndexedColors.BLACK.getIndex());
        f.setBoldweight(Font.BOLDWEIGHT_BOLD);

        // 创建第二种字体样式（用于值）
        f2.setFontHeightInPoints((short) 10);
        f2.setColor(IndexedColors.BLACK.getIndex());

        // 设置第一种单元格的样式（用于列名）
        cs.setFont(f);
        cs.setBorderLeft(CellStyle.BORDER_THIN);
        cs.setBorderRight(CellStyle.BORDER_THIN);
        cs.setBorderTop(CellStyle.BORDER_THIN);
        cs.setBorderBottom(CellStyle.BORDER_THIN);
        cs.setAlignment(CellStyle.ALIGN_CENTER);

        // 设置第二种单元格的样式（用于值）
        cs2.setFont(f2);
        cs2.setBorderLeft(CellStyle.BORDER_THIN);
        cs2.setBorderRight(CellStyle.BORDER_THIN);
        cs2.setBorderTop(CellStyle.BORDER_THIN);
        cs2.setBorderBottom(CellStyle.BORDER_THIN);
        cs2.setAlignment(CellStyle.ALIGN_CENTER);
        int startNum = 0;
        int headNum = 0;
        if(null!=type && "full".equals(type)){
            startNum = 2;
            headNum = 1;
        }
        //设置列名
        for (int i = headNum; i < columnNames.length; i++) {
            Cell cell = row.createCell(i-headNum);
            cell.setCellValue(columnNames[i]);
            cell.setCellStyle(cs);
        }
        //冻结 第一个参数 列，第二个参数 列
        sheet.createFreezePane(0,1);
        //设置每行每列的值
        for (short i = 0; i < columnValues.length; i++) {
            // Row 行,Cell 方格 , Row 和 Cell 都是从0开始计数的
            String cc = columnValues[i];
            if(null==cc){
                continue;
            }
            // 创建一行，在页sheet上
            Row row1 = sheet.createRow(i);
            String [] cells = columnValues[i].split("@");
            // 在row行上创建一个方格
            for (int j = startNum; j < cells.length; j++) {
                Cell cell = row1.createCell(j-startNum);
                cell.setCellValue(cells[j]);
                cell.setCellStyle(cs2);
            }
        }
        sheet.setDefaultColumnWidth(20);
//        sheet.setColumnWidth(0,10);
//        sheet.setColumnWidth(1,10);
        return wb;
    }
}
