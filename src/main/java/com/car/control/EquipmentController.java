package com.car.control;

import com.alibaba.fastjson.JSON;
import com.car.service.EquipentService;
import com.car.vo.EquipmentVO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * Created by yinqing on 2017/10/21 0021.
 */
@Controller
public class EquipmentController {
    @Autowired
    private EquipentService service;

    private static Logger logger  = Logger.getLogger(EquipmentController.class);

    /**
     * 查询终端设备信息
     * @param eq
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/selectEquipmentByParmas.do", method = RequestMethod.POST)
    public void selectEquipmentByParmas(EquipmentVO eq,
                  HttpServletRequest request, HttpServletResponse response) throws Exception{
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        try {
            List<EquipmentVO> list = service.selectEquipmentByParmas(eq);
            json = JSON.toJSON(list).toString();
            json = "{\"success\":true,\"dataCount\":"+list.size()+",\"data\":"+json+"}";
            logger.info("查询所有终端设备信息总数======"+list.size());
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":"+e+"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 查询所有未被绑定的终端
     * @param eq
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/selectNewEquipment.do", method = RequestMethod.POST)
    public void selectNewEquipment(EquipmentVO eq,
                                        HttpServletRequest request, HttpServletResponse response) throws Exception{
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        try {
            List<EquipmentVO> list = service.selectNewEquipment(eq);
            json = JSON.toJSON(list).toString();
            json = "{\"success\":true,\"dataCount\":"+list.size()+",\"data\":"+json+"}";
            logger.info("查询所有终端设备信息总数======"+list.size());
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":"+e+"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 新增终端设备信息
     * @param eqVo
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/addEquipment.do" ,method = RequestMethod.POST)
    public void addEquipment(EquipmentVO eqVo,
                         HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        String msg = "";
            logger.info("新增参数===="+eqVo);
        try{
            Date currTime = new Date();
            eqVo.setInDate(currTime);
            int num = service.addEquipment(eqVo);
            if(num==1){
                msg = "设备号已存在";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }else if(num==2){
                msg = "SIM卡号已存在";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }else if(num == 0){
                msg = "添加成功";
                json = "{\"success\":true,\"message\":\""+msg+"\"}";
            }else{
                msg = "系统繁忙，请稍后再试";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }
        }catch (Exception e){
            e.printStackTrace();
            msg = "系统繁忙，请稍后再试";
            json = "{\"success\":false,\"message\":\""+msg+"\"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 修改终端设备信息
     * @param eq
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/updateEquipment.do" ,method = RequestMethod.POST)
    public void updateEquipment(EquipmentVO eq,
                        HttpServletRequest request,HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        String msg = "";
        logger.info("修改终端设备ID====="+eq.getId()+"===终端设备号："+eq.getSn());
        try{
            int num = service.updateEquipment(eq);
            if(num==1){
                msg = "设备号已存在";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }else if(num==2){
                msg = "SIM卡号已存在";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }else if(num == 0){
                msg = "修改成功";
                json = "{\"success\":true,\"message\":\""+msg+"\"}";
            }else{
                msg = "系统繁忙，请稍后再试";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }
        }catch (Exception e){
            e.printStackTrace();
            msg = "系统繁忙，请稍后再试";
            json = "{\"success\":false,\"message\":\""+msg+"\"}";

        }
        response.getWriter().write(json);
    }

    @RequestMapping(value = "/deleteEquipment.do",method = RequestMethod.POST)
    public void deleteEquipment( String ids,String personToEqId,
                     HttpServletRequest request, HttpServletResponse response) throws Exception{
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        try{
            String[] eqIds = ids.split(",");
            for (String id:eqIds){
                int eqId = Integer.parseInt(id);
                service.deleteEquipmentById(eqId);
            }
            String[] pToEqId = personToEqId.split(",");
            if(pToEqId.length!=0){
                for (String pId:pToEqId){
                    int eqId = Integer.parseInt(pId);
                    service.deletePersonToEquipmentById(eqId);
                }
            }
            response.getWriter().write("true");
        }catch (Exception e){
            e.printStackTrace();
            response.getWriter().write("false");
        }
    }
}
