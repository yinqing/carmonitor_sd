package com.car.control;

import com.car.util.CommUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import com.car.bean.User;

import com.car.service.UserService;
//import com.beidouapp.util.CommUtil;
//import com.beidouapp.web.bean.LoginUser;

@Controller
@RequestMapping(value = "/hello", method = RequestMethod.GET)
public class HelloController {
    @Autowired
    private UserService service;
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String printHello(ModelMap model) {
        model.addAttribute("msg", "Spring MVC Hello World");
        model.addAttribute("name", "yuntao");
        System.out.println("test");
        return "hello";
    }

    /**
     * 用户登录
     * @param userName
     * @param password
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value="/loginzt",method=RequestMethod.GET)
    public void login(String userName, String password, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");

        try {
           // LoginUser loginUser = CommUtil.getLoginUserFromSession(request);
            //if(loginUser == null) {
                User user = service.findUserByNameAndPassword("ytxy", CommUtil.generateMD5Str("123456"));
                System.out.println("test2" + user);
                if(user != null) {
                  //  this.setLoginUser(user, request);
                    response.getWriter().write("true");
                } else {
                    response.getWriter().write("false");
                }
            //} else if(loginUser.getUserName().equals(userName.trim())) {
              //  response.getWriter().write("true");
           // } else {
             //   response.getWriter().write("{\"success\":\"false\",\"message\":\"已有用户登录了，不能同时登录多个用户！\"}");
            //}
        } catch (Exception e) {
            e.printStackTrace();
            response.getWriter().write("false");
        }
    }
}