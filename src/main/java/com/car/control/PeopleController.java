package com.car.control;

import com.alibaba.fastjson.JSON;
import com.car.service.PeopleService;
import com.car.service.RouteService;
import com.car.vo.PeopleVO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;
import java.util.UUID;

@Controller
public class PeopleController {
    @Autowired
    private PeopleService service;
    @Autowired
    private RouteService routeService;
    private static Logger logger  = Logger.getLogger(PeopleController.class);

    /**
     * 查询所有人员信息
     * @param pVo
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/selectPersonByParams.do", method = RequestMethod.POST)
    public void selectPersonByParams(PeopleVO pVo,
                                     HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        try {
            List<PeopleVO> list = service.selectPersonByParams(pVo);
            json = JSON.toJSON(list).toString();
            json = "{\"success\":true,\"dataCount\":"+list.size()+",\"data\":"+json+"}";
            logger.info("查询所有人员信息总数======"+list.size());
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":"+e+"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 新增人员信息
     * @param pVo
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/addPerson.do", method = RequestMethod.POST)
    public void addPerson(PeopleVO pVo,MultipartFile file,
                      HttpServletRequest request,HttpServletResponse response) throws Exception{
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        String msg = "";
        //上传图片的原始名称
        String originalFilename = file.getOriginalFilename();
        try{
            //上传图片
            if(originalFilename!=null&&originalFilename.length()>0) {

                //存储图片的物理路径
                String pic_path = "E:\\MyProject\\temp\\";

                //新的图片名称
                String newFileName = UUID.randomUUID() + originalFilename.substring(originalFilename.lastIndexOf("."));
                //将新的文件名赋值给pVo
                pVo.setPhotoImg(newFileName);

                File newFile = new File(pic_path + newFileName);

                //将内存中的数据写入磁盘
                file.transferTo(newFile);
            }

            int num = service.addPerson(pVo);
            if(num==1){
                msg = "手机号已存在";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }else if(num == 0){
                msg = "添加成功";
                json = "{\"success\":true,\"message\":\""+msg+"\"}";
            }else{
                msg = "系统繁忙，请稍后再试";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }
        }catch (Exception e){
            e.printStackTrace();
            msg = "系统繁忙，请稍后再试";
            json = "{\"success\":false,\"message\":\""+msg+"\"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 修改人员信息
     * @param pVo
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/updatePerson.do",method = RequestMethod.POST)
    public void updatePerson(PeopleVO pVo,MultipartFile file,
                         HttpServletRequest request, HttpServletResponse response) throws Exception{
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        String msg = "";
        //上传图片的原始名称
        String originalFilename = file.getOriginalFilename();
        try{
            //上传图片
            if(originalFilename!=null&&originalFilename.length()>0) {


                //存储图片的物理路径
                String pic_path = "E:\\MyProject\\temp\\";
                //如果之前有图片，则删了重新上传
                if(pVo.getPhotoImg().length()>0){
                    File oldFile = new File(pic_path + pVo.getPhotoImg());
                    oldFile.delete();
                }

                //新的图片名称
                String newFileName = UUID.randomUUID() + originalFilename.substring(originalFilename.lastIndexOf("."));
                //将新的文件名赋值给pVo
                pVo.setPhotoImg(newFileName);

                File newFile = new File(pic_path + newFileName);

                //将内存中的数据写入磁盘
                file.transferTo(newFile);
            }

            int num = service.updatePerson(pVo);
            if(num==1){
                msg = "手机号已存在";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }else if(num == 0){
                msg = "修改成功";
                json = "{\"success\":true,\"message\":\""+msg+"\"}";
                routeService.updateEquipmentNum(pVo.getId(),pVo.getEquipmentNo());
            }else{
                msg = "系统繁忙，请稍后再试";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }
        }catch (Exception e){
            e.printStackTrace();
            msg = "系统繁忙，请稍后再试";
            json = "{\"success\":false,\"message\":\""+msg+"\"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 删除人员信息
     * @param ids
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/deletePersonById.do", method = RequestMethod.POST)
    public void deletePersonById(String ids,String personToEqIds,
                         HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        try{
            String[] eqIds = ids.split(",");
            for (String id:eqIds){
                int pId = Integer.parseInt(id);
                service.deletePersonById(pId);
            }
            String[] pToEqIds = personToEqIds.split(",");
            if(pToEqIds.length!=0){
                for(String eqId:pToEqIds){
                    int pToEqId = Integer.parseInt(eqId);
                    service.deletePersonToEqById(pToEqId);
                }
            }
            response.getWriter().write("true");
        }catch (Exception e){
            e.printStackTrace();
            response.getWriter().write("false");
        }

    }
    @RequestMapping(value = "/uploadFile.do",method = RequestMethod.POST)
    public void  uploadFile(PeopleVO pVo,MultipartFile file,
                            HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        //上传图片的原始名称
        String originalFilename = file.getOriginalFilename();
        try {
            //上传图片
            if(originalFilename!=null&&originalFilename.length()>0){

                //存储图片的物理路径
                String pic_path="E:\\MyProject\\temp\\";

                //新的图片名称
                String newFileName= UUID.randomUUID()+originalFilename.substring(originalFilename.lastIndexOf("."));

                File newFile=new File(pic_path+newFileName);

                //将内存中的数据写入磁盘
                file.transferTo(newFile);
                response.getWriter().write("成功了");

            }
        }catch (Exception e){
            e.printStackTrace();
            response.getWriter().write("失败了");
        }
    }
}
