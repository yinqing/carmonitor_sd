package com.car.control;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.car.service.CurrentPositionService;
import com.car.vo.CurrentPositionVO;
import com.car.vo.PageParam;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 实时位置
 * Created by yinqing on 2017/10/25.
 */
@Controller
public class CurrentPositionController {

    @Autowired
    private CurrentPositionService service;

    private static Logger logger = Logger.getLogger(CurrentPositionController.class);

    @RequestMapping(value = "/selectCurrPosition.do", method = RequestMethod.POST)
    public void selectCurrPosition(CurrentPositionVO currVo,
                                   HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        String json = "";

        try {
            List<CurrentPositionVO> list = service.selectCurrPosition(currVo);
            json = JSON.toJSON(list).toString();
//            SimpleDateFormat timeFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            Date now=new Date();
//            String currTime = timeFormat.format(now);
            long currTime = new Date().getTime();
            json = "{\"success\":true,\"dataCount\":"+list.size()+",\"currTime\":\""+currTime+"\",\"data\":"+json+"}";
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":\""+e+"\"}";
        }

        response.getWriter().write(json);

    }
}
