package com.car.control;

import com.alibaba.fastjson.JSON;
import com.car.service.TowerService;
import com.car.vo.TowerVO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Administrator on 2017/10/23 0023.
 */
@Controller
public class TowerController {
    @Autowired
    private TowerService service;
    public static  Logger logger  = org.apache.log4j.Logger.getLogger(TowerController.class);

    @RequestMapping(value = "/selectAllTower.do", method= RequestMethod.POST)
    public void selectAllTower(TowerVO towerVO,
                               HttpServletRequest request, HttpServletResponse response) throws Exception{
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        try{
            List<TowerVO> list = service.selectAllTower(towerVO);
            json = JSON.toJSON(list).toString();
            json = "{\"success\":true,\"dataCount\":"+list.size()+",\"data\":"+json+"}";
            logger.info("查询所有终端设备信息总数======"+list.size());
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":"+e+"}";
        }
        response.getWriter().write(json);
    }
    @RequestMapping(value = "/addTower.do", method = RequestMethod.POST)
    public void addTower(TowerVO towerVO,
                         HttpServletRequest request,HttpServletResponse response) throws Exception{
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        try{
            service.addTower(towerVO);
            response.getWriter().write("true");
        }catch (Exception e){
            e.printStackTrace();
            response.getWriter().write("false");
        }
    }
    @RequestMapping(value = "/updateTower.do", method = RequestMethod.POST)
    public void updateTower(TowerVO towerVO,
                            HttpServletRequest request,HttpServletResponse response) throws Exception{
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        try{
            service.updateTower(towerVO);
            response.getWriter().write("true");
        }catch (Exception e){
            e.printStackTrace();
            response.getWriter().write("false");
        }
    }
    @RequestMapping(value = "/deleteTowerById.do", method = RequestMethod.POST)
    public void deleteTowerById(String ids,
                                HttpServletRequest request,HttpServletResponse response) throws Exception{
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        try{
            String[] eqIds = ids.split(",");
            for (String id:eqIds){
                int tId = Integer.parseInt(id);
                service.deleteTowerById(tId);
            }
            response.getWriter().write("true");
        }catch (Exception e){
            e.printStackTrace();
            response.getWriter().write("false");
        }
    }

    @RequestMapping(value = "/selectTowerByRouteId.do",method = RequestMethod.POST)
    public void selectTowerByRouteId(String id, HttpServletRequest request,HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        try{
            int routeId = Integer.parseInt(id);
            List<TowerVO> list = service.selectTowerByRouteId(routeId);
            json = JSON.toJSON(list).toString();
            json = "{\"success\":true,\"dataCount\":"+list.size()+",\"data\":"+json+"}";
            logger.info("查询线路"+id+"终端设备信息总数======"+list.size());
        }catch (Exception e){
            e.printStackTrace();
        }
        response.getWriter().write(json);
    }
    @RequestMapping(value = "/selectTowerByTaskId.do",method = RequestMethod.POST)
    public void selectTowerByTaskId(String id, HttpServletRequest request, HttpServletResponse response) throws Exception{
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        String msg = "";
        try{
            logger.info("要查询的作业Id是：==="+id);
            Integer taskId = Integer.parseInt(id);
            List<TowerVO> list = service.selectTowerByTaskId(taskId);
            json = JSON.toJSON(list).toString();
            json = "{\"success\":true,\"dataCount\":"+list.size()+",\"data\":"+json+"}";
            logger.info("根据taskId查询杆塔总数======"+list.size());

        }catch (Exception e){
            e.printStackTrace();
            logger.error("根据taskId查询杆塔总数异常===="+e);
            msg = "系统繁忙，请稍后再试";
            json = "{\"success\":false,\"message\":\""+msg+"\"}";
        }
        response.getWriter().write(json);
    }
}
