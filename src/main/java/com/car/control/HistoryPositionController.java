package com.car.control;

import com.alibaba.fastjson.JSON;
import com.car.service.HistoryPositionService;
import com.car.vo.HistoryPositionVO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 历史查询 包括轨迹查询
 * Created by yinqing on 2017/12/16 0016.
 */
@Controller
public class HistoryPositionController {
    @Autowired
    private HistoryPositionService service;
    private static Logger logger  = Logger.getLogger(HistoryPositionController.class);

    /**
     * 查询轨迹信息
     * @param eqNum 设备编号
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/selectGJ.do", method = RequestMethod.POST)
    public void selectGJ(String eqNum,HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        try{
           List<HistoryPositionVO> list =  service.selectGJ(eqNum);
            json = JSON.toJSON(list).toString();
            json = "{\"success\":true,\"dataCount\":"+list.size()+",\"data\":"+json+"}";
            logger.info("查询终端编号为"+eqNum+"的轨迹信息总数======"+list.size());
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":"+e+"}";
        }
        response.getWriter().write(json);
    }


}
