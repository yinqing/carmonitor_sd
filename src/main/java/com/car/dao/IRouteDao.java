package com.car.dao;

import com.car.vo.RouteVO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/10/24.
 */
public interface IRouteDao {
    //未巡检的
    List<RouteVO> selectAllRoute(RouteVO routeVO) throws Exception;
    //已巡检的
    List<RouteVO> selectRouteOld(RouteVO routeVO) throws Exception;

    int addRoute(RouteVO routeVO) throws Exception;

    void addRoutePeople(HashMap<String, Integer> map) throws Exception;

    void addRouteTower(HashMap<String, Integer> map) throws Exception;

    void updateRoute(RouteVO routeVO) throws Exception;

    void deleteRouteById(int id) throws Exception;

    List<RouteVO> selectPeopleInfo(int id) throws Exception;

    List<RouteVO> selectTowerInfo(int id) throws Exception;

    /**
     * 往route_relation关系表中插值
     * @param map
     * @throws Exception
     */
    void addRouteRelation(HashMap<String, String> map) throws Exception;

    /**
     * 删除route_relation中的关联信息
     * @param id
     * @throws Exception
     */
    void deleteRouteRelationById(int id) throws Exception;

    //根据杆塔信息查询所有线路名称
    List<RouteVO> selectRouteByTowerId(int id) throws Exception;

    List<RouteVO> selectRouteStatusByTowerId(int id) throws Exception;

    int selectRouteRalationByPeopleId(Integer id) throws Exception;

    //根据人员Id，更改eqnum信息
    void updateRouteRelationByPeopleId(HashMap<String, String> map);

    //查询已完成的线路信息
    List<RouteVO> checkRouteStatus() throws Exception;

    void moveDataToOld(RouteVO routeVO) throws Exception;

    //route_old表也删一遍
    void deleteRouteOldByRouteId(int routeId) throws Exception;
}
