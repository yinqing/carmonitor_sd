package com.car.dao;

import com.car.vo.WarnVO;

import java.util.List;

public interface IWarnDao {
    List<WarnVO> selectWarnInfo(WarnVO warnVO) throws Exception;

    void updateWarnInfo(String id) throws Exception;
}
