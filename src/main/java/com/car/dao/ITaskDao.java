package com.car.dao;

import com.car.vo.TaskVO;

import java.util.HashMap;
import java.util.List;

public interface ITaskDao {
    List<TaskVO> selectAllTask(TaskVO taskVO) throws Exception;



    void addTaskTower(HashMap<String, String> map) throws Exception;

    void addTaskPeople(HashMap<String, String> map) throws Exception;

    void updateTask(TaskVO taskVO) throws Exception;

    void deleteTaskTowerById(Integer taskId) throws Exception;

    void deleteTaskPeopleById(Integer taskId) throws Exception;

    void deleteTaskById(int id) throws Exception;

    Integer selectLastTaskId() throws Exception;

    void addTask(TaskVO taskVO) throws Exception;
    //根据线路名称 精准查询信息
    List<TaskVO> selectAllTaskByParams(TaskVO newVo) throws Exception;
    //根据peopleId查询作业信息
    List<TaskVO> selectTaskByPId(Integer pId) throws Exception;
}
