package com.car.dao;

import com.car.vo.CurrentPositionVO;

import java.util.List;

/**
 * 实时位置
 * Created by yinqing on 2017/10/25.
 */
public interface ICurrentPositionDao {
    List<CurrentPositionVO> selectCurrPosition(CurrentPositionVO currVo) throws Exception;
}
