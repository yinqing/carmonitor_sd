package com.car.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

public class FileDownloadHelper {

    //公用文件下载处理
    public static void download(String filepath, String filename, String alias,
                                HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setHeader("Content-Type","application/force-download");
        if(CommUtil.isNotEmptyStr(alias))
            response.setHeader("Content-Disposition","attachment;filename=" + URLEncoder.encode(alias, "utf-8"));
        else
            response.setHeader("Content-Disposition","attachment;filename=" + URLEncoder.encode(filename, "utf-8"));

        FileInputStream in = new FileInputStream(filepath + "\\" + filename );
        OutputStream out = response.getOutputStream();

        byte buffer[] = new byte[1024];
        int len = 0;
        while((len = in.read(buffer))>0) {
            out.write(buffer,0,len);
        }
        in.close();
        out.close();
    }

    //模板文件下载处理
    public static void downloadTemplate(String path, String filename, String alias,
                                        HttpServletRequest request, HttpServletResponse response) throws Exception {
        String filepath = request.getSession().getServletContext().getRealPath("/WEB-INF/download/" + path);
        download(filepath,filename,alias,request,response);
    }
}
